<?php

namespace app\models\api\forms;

use app\models\Comment;
use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class CommentForm extends Model
{
    public $text;
    public $page;
	public $answer_to;
	public $is_public;

    public function rules() {
        return [
	        ['text', 'required'],
	        [['text', 'page'], 'string'],
	        ['is_public', 'boolean'],
	        ['answer_to', 'exist', 'targetClass' => Comment::className(), 'targetAttribute' => 'id'],
        ];
    }

	public function createComment() {


	}
}
