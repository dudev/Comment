<?php

namespace app\models\api\search;

use app\models\Site;
use Yii;
use yii\base\Model;

/**
 * ProductSearch represents the model behind the search form about `app\models\Product`.
 */
class SiteSearch extends Site
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 * @return array
	 */
    public function search($params) {
	    $query = Site::find()
		    ->select(['id', 'name'])
	        ->with('domains');

	    // load the search form data and validate
	    if ($this->load($params) && $this->validate()) {
		    $query->andFilterWhere([
			    'id' => $this->id,
			    'user_id' => $this->user_id,
		    ]);
	    }

	    $sites = [];
	    foreach ($query->all() as $val) {
		    /* @var $val Site */
		    $sites[] = array_merge(
			    $val->getAttributes(['id', 'name']),
			    [
				    'domains' => Site::getAllDomains($val),
			    ]
		    );
	    }

	    return [
		    'sites' => $sites,
	    ];
    }
}
