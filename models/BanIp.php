<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ban_ip".
 *
 * @property integer $id
 * @property string $ip
 * @property integer $site_id
 * @property integer $owner_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $owner
 * @property Site $site
 */
class BanIp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ban_ip';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip', 'site_id', 'created_at', 'updated_at'], 'required'],
            [['site_id', 'owner_id', 'created_at', 'updated_at'], 'integer'],
            [['ip'], 'string', 'max' => 39],
            [['ip', 'site_id'], 'unique', 'targetAttribute' => ['ip', 'site_id'], 'message' => 'The combination of Ip and Site ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'Ip',
            'site_id' => 'Site ID',
            'owner_id' => 'Owner ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }
}
