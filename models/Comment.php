<?php

namespace app\models;

use app\extensions\ApiController;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property string $text
 * @property integer $is_public
 * @property string $ip
 * @property string $browser
 * @property integer $user_id
 * @property integer $page_id
 * @property integer $answer_to
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Comment $answerTo
 * @property Comment[] $comments
 * @property Page $page
 * @property User $user
 */
class Comment extends ActiveRecord
{
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'ip', 'browser', 'page_id'], 'required'],
            [['text'], 'string'],
            [['user_id', 'page_id', 'answer_to'], 'integer'],
            [['ip'], 'string', 'max' => 39],
            [['browser'], 'string', 'max' => 255],
	        ['user_id', 'exist', 'targetClass' => User::className(), 'targetAttribute' => 'id', 'when' => function($model) {
		        /* @var $model Comment */
		        return !empty($model->user_id);
	        }],
	        ['page_id', 'exist', 'targetClass' => Page::className(), 'targetAttribute' => 'id'],
	        [
		        'answer_to',
		        'filter',
		        'filter' => function($value) {
			        /* @var Comment $model */
			        $exist = Comment::find()
				        ->where([
					        'id' => $this->answer_to,
					        'page_id' => $this->page_id,
				        ])
				        ->exists();
			        if(!$exist) {
				        return null;
			        }
			        return $value;
		        },
		        'when' => function($model) {
			        return !is_null($model->answer_to);
		        },
	        ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Text',
            'is_public' => 'Is Public',
            'ip' => 'Ip',
            'browser' => 'Browser',
            'user_id' => 'User ID',
            'page_id' => 'Page ID',
            'answer_to' => 'Answer To',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswerTo()
    {
        return $this->hasOne(Comment::className(), ['id' => 'answer_to']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['answer_to' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

	public static function getStructuredComments($url) {
		if($url) {
			if(!empty($url['host'])
				&& $domain = Domain::findOne(['domain' => $url['host']])) {
				/** @var $domain Domain */
				$page = $url['path'];
				if(isset($url['query'])) {
					parse_str($url['query'], $query);
					if ($query) {
						$values = [];
						foreach (json_decode($domain->site->get_param) as $v) {
							if (isset($query[$v]))
								$values[$v] = $query[$v];
						}
						$page .= '?' . http_build_query($values);
					}
				}
				if($model_page = Page::findOne(['path' => $page, 'site_id' => $domain->site->id])) {
					$model_comments = Comment::find()
						->where(['page_id' => $model_page->id ])
						->orderBy(['id' => SORT_DESC])
						->all();
					if($model_comments) {
						$comments = [];
						foreach ($model_comments as $comment) {
							$comments[ $comment->id ] = array_merge(
								$comment->getAttributes(['id', 'text']),
								ApiController::isAdmin($domain->site) ? ['is_public' => $comment->is_public] : [],
								['page' => $page],
								['username' => empty($comment->user) ? 'Анонимно' : $comment->user->nick]
							);
						}
						if($domain->site->enable_tree_view) {
							foreach ($comments as $id => $comment) {
								$comments[$comment->answer_to]['comments'][] = $comment;
								unset($comments[$id]);
							}
						}

						return array_values($comments);
					}
				}
				return [];
			}
		}
		return null;
	}
	public static function createComment($url, $domain, $form) {
		/** @var $domain Domain */
		if(!$domain->site->enable_anonym_comment && Yii::$app->user->isGuest) {
			return false;
		}

		$page = $url['path'];
		if(isset($url['query'])) {
			parse_str($url['query'], $query);
			if ($query) {
				$values = [];
				foreach (json_decode($domain->site->get_param) as $v) {
					if (isset($query[$v]))
						$values[$v] = $query[$v];
				}
				$page .= '?' . http_build_query($values);
			}
		}

		if(!$model_page = Page::findOne(['path' => $page, 'site_id' => $domain->site->id])) {
			$model_page = new Page();
			$model_page->site_id = $domain->site->id;
			$model_page->path = $page;
			$model_page->save();
		}

		/* @var $form Comment */
		if($form->load(\Yii::$app->request->post())) {
			$form->page_id = $model_page->id;
			$form->user_id = Yii::$app->user->id;
			$form->is_public = (int)!$domain->site->pre_moderation;
			$form->ip = Yii::$app->request->getUserIP();
			$form->browser = Yii::$app->request->getUserAgent();

			if ($form->save()) {
				return true;
			}
		}
		return false;
	}
}
