<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "site".
 *
 * @property integer $id
 * @property string $name
 * @property integer $enable_anonym_comment
 * @property string $get_param
 * @property integer $pre_moderation
 * @property integer $enable_tree_view
 * @property string $secret
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property BanIp[] $banIps
 * @property BanUser[] $banUsers
 * @property Domain[] $domains
 * @property Page[] $pages
 * @property User $user
 */
class Site extends ActiveRecord
{
	public $domain;

	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'get_param', 'pre_moderation', 'enable_tree_view', 'user_id'], 'required'],
            [['enable_anonym_comment', 'pre_moderation', 'enable_tree_view', 'user_id'], 'integer'],
            [['name', 'get_param'], 'string', 'max' => 255],
            ['secret', 'string', 'max' => 30],
	        ['domain', 'validateDomains'],
	        ['get_param', 'filter', 'filter' => function($value) {
		        $value = is_array($value) ? $value : explode(',', $value);
		        $value = array_filter($value, function($val){
			        return (bool)trim($val);
		        });
		        if($value) {
			        sort($value, SORT_STRING);
		        }
		        return json_encode($value);
	        }],
        ];
    }

	public function validateDomains($attribute, $params) {
		$this->domain = is_array($this->domain) ? $this->domain : explode(',', $this->domain);
		$this->domain = array_filter($this->domain, function($val){
			return (bool)trim($val);
		});

		if(!$this->domain) {
			$this->addError('domain', 'Необходимо указать хотя бы 1 домен.');
		}

		foreach ($this->domain as &$domain) {
			$domain = mb_strtolower($domain);

			if(!preg_match('#^[a-z0-9-]+(\.[a-z0-9-]+)+$#i', $domain)) {
				$this->addError('domain', 'Неверный формат домена.');
				return;
			}

			if(Domain::findOne([
				'AND',
				['domain' => $domain],
				['!=', 'site_id', $this->id],
			])) {
				$this->addError('domain', 'Домен уже используется.');
				return;
			}
		}
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'enable_anonym_comment' => 'Enable Anonym Comment',
            'get_param' => 'Ignore Get Param',
            'pre_moderation' => 'Pre Moderation',
            'enable_tree_view' => 'Enable Tree View',
            'secret' => 'Secret',
            'user_id' => 'User ID',
        ];
    }

	/**
	 * @param $model Site
	 * @return array
	 */
	public static function getAllDomains($model) {
		$domains = [];
		foreach ($model->getDomains() as $v) {
			/* @var $v \app\models\Domain */
			$domains[] = $v->domain;
		}
		return $domains;
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanIps()
    {
        return $this->hasMany(BanIp::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanUsers()
    {
        return $this->hasMany(BanUser::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDomains()
    {
        return $this->hasMany(Domain::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(Page::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
