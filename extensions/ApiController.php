<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\extensions;
use general\controllers\api\Controller;

/**
 * Class Controller
 */
class ApiController extends Controller {
	/**
	 * @param $site \app\models\Site
	 * @return bool
	 */
	public static function isAdmin($site) {
		if(\Yii::$app->user->isGuest) {
			return false;
		}
		$user_id = \Yii::$app->user->id;
		if($site->user_id == $user_id) {
			return true;
		}
		/*if($redactors = $this->getSite()->redactors) {
			foreach ($redactors as $redactor) {
				/* @var \app\models\Redactor $redactor * /
				if($redactor->site_id == $user_id) {
					return true;
				}
			}
		}*/
		return false;
	}
} 