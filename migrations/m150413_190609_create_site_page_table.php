<?php

use yii\db\Schema;
use yii\db\Migration;

class m150413_190609_create_site_page_table extends Migration
{
    public function up()
    {
	    $this->createTable('page', [
		    'id' => Schema::TYPE_PK,
		    'path' => Schema::TYPE_STRING . ' NOT NULL',
		    'site_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->addForeignKey('site_id_FK_page', 'page', 'site_id', 'site', 'id', 'CASCADE', 'CASCADE');
	    $this->createIndex('page_tbl_path_site_id_idx', 'page', ['path', 'site_id'], true);
	    $this->createIndex('page_tbl_site_id_idx', 'page', 'site_id');
    }

    public function down()
    {
        echo "m150413_190609_create_site_page_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
