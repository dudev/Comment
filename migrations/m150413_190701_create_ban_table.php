<?php

use yii\db\Schema;
use yii\db\Migration;

class m150413_190701_create_ban_table extends Migration
{
    public function up()
    {
	    $this->createTable('ban_ip', [
		    'id' => Schema::TYPE_PK,

		    'ip' => Schema::TYPE_STRING . '(39) NOT NULL',
		    'site_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'owner_id' => Schema::TYPE_INTEGER,

		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->addForeignKey('owner_id_FK_ban_ip', 'ban_ip', 'owner_id', 'user', 'id', 'SET NULL', 'CASCADE');
	    $this->addForeignKey('site_id_FK_ban_ip', 'ban_ip', 'site_id', 'site', 'id', 'CASCADE', 'CASCADE');
	    $this->createIndex('ban_ip_tbl_ip_site_id_idx', 'ban_ip', ['ip', 'site_id'], true);

	    $this->createTable('ban_user', [
		    'id' => Schema::TYPE_PK,

		    'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'site_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'owner_id' => Schema::TYPE_INTEGER,

		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->addForeignKey('owner_id_FK_ban_user', 'ban_user', 'owner_id', 'user', 'id', 'SET NULL', 'CASCADE');
	    $this->addForeignKey('site_id_FK_ban_user', 'ban_user', 'site_id', 'site', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('user_id_FK_ban_user', 'ban_user', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
	    $this->createIndex('ban_user_tbl_user_id_site_id_idx', 'ban_user', ['user_id', 'site_id'], true);
    }

    public function down()
    {
        echo "m150413_190701_create_ban_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
