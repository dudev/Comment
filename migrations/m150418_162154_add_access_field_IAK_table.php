<?php

use yii\db\Schema;
use yii\db\Migration;

class m150418_162154_add_access_field_IAK_table extends Migration
{
    public function up()
    {
	    $this->addColumn('issued_access_keys', 'access', 'ENUM(\'close\', \'open\') NOT NULL DEFAULT \'open\'');
    }

    public function down()
    {
        echo "m150418_162154_add_access_field_IAK_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
