<?php

use yii\db\Schema;
use yii\db\Migration;

class m150413_190620_create_comment_table extends Migration
{
    public function up()
    {
	    $this->createTable('comment', [
		    'id' => Schema::TYPE_PK,
		    'text' => Schema::TYPE_TEXT . ' NOT NULL',
		    'is_public' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 1',
		    'ip' => Schema::TYPE_STRING . '(39) NOT NULL',
		    'browser' => Schema::TYPE_STRING . ' NOT NULL',
		    'user_id' => Schema::TYPE_INTEGER,
		    'page_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'answer_to' => Schema::TYPE_INTEGER,
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->addForeignKey('user_id_FK_comment', 'comment', 'user_id', 'user', 'id', 'SET NULL', 'CASCADE');
	    $this->addForeignKey('page_id_FK_comment', 'comment', 'page_id', 'page', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('answer_to_FK_comment', 'comment', 'answer_to', 'comment', 'id', 'CASCADE', 'CASCADE');
	    $this->createIndex('comment_tbl_page_id_is_public_idx', 'comment', ['page_id', 'is_public']);
    }

    public function down()
    {
        echo "m150413_190620_create_comment_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
