<?php

use yii\db\Schema;
use yii\db\Migration;

class m150413_190550_create_site_domain_table extends Migration
{
    public function up()
    {
	    $this->createTable('domain', [
		    'domain' => Schema::TYPE_STRING . '(100) NOT NULL',
		    'site_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->addForeignKey('site_id_FK_domain', 'domain', 'site_id', 'site', 'id', 'CASCADE', 'CASCADE');
	    $this->createIndex('domain_tbl_domain_idx', 'domain', 'domain', true);
	    $this->createIndex('domain_tbl_site_id_idx', 'domain', 'site_id');
    }

    public function down()
    {
        echo "m150413_190550_create_site_domain_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
