<?php

namespace app\controllers\api;

use app\extensions\ApiController;
use app\models\api\search\SiteSearch;
use app\models\Domain;
use app\models\Site;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class SiteController extends ApiController {
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['list', 'create', 'delete', 'update'],
				'rules' => [
					[
						'actions' => ['list', 'create', 'delete', 'update'],
						'allow' => true,
						'matchCallback' => function ($rule, $action) {
							return $this->_access_key && $this->_access_key->access == 'close';
						}
					],
				],
			],
		];
	}
	/**
	 * @return array
	 */
    public function actionList() {
	    $data = (new SiteSearch())->search(\Yii::$app->request->get('search', []));

	    if(is_int($data)) {
		    return $this->sendError($data);
	    }

	    if($data === []) {
		    return $this->sendError(self::ERROR_DB);
	    }

	    return $this->sendSuccess($data);
    }
    /**
     * Creates a new Product model.
     * @return array
     */
    public function actionCreate() {
	    if(\Yii::$app->request->isGet) {
		    return $this->sendError(self::ERROR_ILLEGAL_REQUEST_METHOD);
	    }
	    $model = new Site();

	    if(!$model->load(\Yii::$app->request->post())) {
		    return $this->sendError(self::ERROR_NO_DATA);
	    }

	    if($model->validate()) {
		    try {
			    $model->save(false);
			    $this->createDomains($model->domain, $model);

		    } catch(Exception $e) {
			    return $this->sendError(self::ERROR_DB);
		    }
		    return $this->sendSuccess([
			    'site' => array_merge(
				    $model->getAttributes(null, ['updated_at', 'created_at']),
				    ['domains' => Site::getAllDomains($model)]
			    )
		    ]);
	    } else {
		    $errors = $this->getErrorCodes([
			    'name' => self::ERROR_ILLEGAL_SITE_NAME_COMMENT,
			    'enable_anonym_comment' => self::ERROR_ILLEGAL_SITE_ANONYM_COMMENT,
			    'get_param' => self::ERROR_ILLEGAL_SITE_GET_PARAMS,
			    'pre_moderation' => self::ERROR_ILLEGAL_PRE_MODERATION,
			    'enable_tree_view' => self::ERROR_ILLEGAL_TREE,
			    'secret' => self::ERROR_ILLEGAL_SECRET_KEY,
			    'user_id' => self::ERROR_ILLEGAL_OWNER,
			    'domain' => self::ERROR_ILLEGAL_SITE_DOMAIN_COMMENT,
		    ], $model);
	    }

	    if(!isset($errors)) {
		    $errors = self::ERROR_UNKNOWN;
	    }
	    return $this->sendError($errors);
    }
    /**
     * Updates an existing Product model.
     * @param integer $id
     * @return array
     */
    public function actionUpdate($id) {
	    /* @var Site $model */
	    $model = Site::findOne($id);
	    if(!$model) {
		    return $this->sendError(self::ERROR_NO_SITE_COMMENT);
	    }
	    if(\Yii::$app->request->isPost) {
		    $oldDomains = [];
		    foreach($model->domains as $val) {
			    $oldDomains[] = $val->domain;
		    }

		    if (!$model->load(\Yii::$app->request->post())) {
			    return $this->sendError(self::ERROR_NO_DATA);
		    }
		    if ($model->validate()) {
			    try {
				    $model->save(false);

				    $diffDelete = array_diff($oldDomains, $model->domain);
				    $diffCreate = array_diff($model->domain, $oldDomains);

				    $this->createDomains($diffCreate, $model);
				    $this->deleteDomains($diffDelete);
			    } catch (Exception $e) {
				    return $this->sendError(self::ERROR_DB);
			    }
		    } else {
			    $errors = $this->getErrorCodes([
				    'name' => self::ERROR_ILLEGAL_SITE_NAME_COMMENT,
				    'enable_anonym_comment' => self::ERROR_ILLEGAL_SITE_ANONYM_COMMENT,
				    'get_param' => self::ERROR_ILLEGAL_SITE_GET_PARAMS,
				    'pre_moderation' => self::ERROR_ILLEGAL_PRE_MODERATION,
				    'enable_tree_view' => self::ERROR_ILLEGAL_TREE,
				    'secret' => self::ERROR_ILLEGAL_SECRET_KEY,
				    'user_id' => self::ERROR_ILLEGAL_OWNER,
				    'domains' => self::ERROR_ILLEGAL_SITE_DOMAIN_COMMENT,
			    ], $model);
			    return $this->sendError($errors);
		    }
	    }

	    return $this->sendSuccess([
		    'site' => array_merge(
			    $model->getAttributes(null, ['updated_at', 'created_at']),
			    ['domains' => Site::getAllDomains($model)]
		    )
	    ]);
    }
    /**
     * Deletes an existing Product model.
     * @param integer $id
     * @return array
     */
    public function actionDelete($id) {
	    /* @var Site $model */
	    if($model = Site::findOne($id)) {
		    try {
			    $oldDomains = [];
			    foreach($model->domains as $val) {
				    $oldDomains[] = $val->domain;
			    }

			    $this->deleteDomains($oldDomains);
			    $model->delete();

			    return $this->sendSuccess([]);
		    } catch (Exception $e) {
			    return $this->sendError(self::ERROR_DB);
		    }
	    } else {
		    return $this->sendError(self::ERROR_NO_PRODUCT);
	    }
    }
	private function createDomains($domains, $site) {
		foreach ($domains as $domain) {
			$d = new Domain();
			$d->domain = $domain;
			$d->site_id = $site->id;
			$d->save();
		}
	}
	private function deleteDomains($domains) {
		foreach ($domains as $domain) {
			if ($d = Domain::findOne(['domain' => $domain])) {
				/* @var $d Domain */
				$d->delete();
			}
		}
	}
}
